package cn.ibizlab.pms.core.zentao.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.pms.core.zentao.domain.Im_conferenceaction;
import cn.ibizlab.pms.core.zentao.filter.Im_conferenceactionSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Im_conferenceactionMapper extends BaseMapper<Im_conferenceaction>{

    Page<Im_conferenceaction> searchDefault(IPage page, @Param("srf") Im_conferenceactionSearchContext context, @Param("ew") Wrapper<Im_conferenceaction> wrapper) ;
    @Override
    Im_conferenceaction selectById(Serializable id);
    @Override
    int insert(Im_conferenceaction entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Im_conferenceaction entity);
    @Override
    int update(@Param(Constants.ENTITY) Im_conferenceaction entity, @Param("ew") Wrapper<Im_conferenceaction> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

}
