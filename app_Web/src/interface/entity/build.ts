/**
 * build
 *
 * @export
 * @interface Build
 */
export interface Build {

    /**
     * 名称编号
     *
     * @returns {*}
     * @memberof Build
     */
    name?: any;

    /**
     * 构建者
     *
     * @returns {*}
     * @memberof Build
     */
    builder?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof Build
     */
    desc?: any;

    /**
     * id
     *
     * @returns {*}
     * @memberof Build
     */
    id?: any;

    /**
     * 已删除
     *
     * @returns {*}
     * @memberof Build
     */
    deleted?: any;

    /**
     * 源代码地址
     *
     * @returns {*}
     * @memberof Build
     */
    scmpath?: any;

    /**
     * 下载地址
     *
     * @returns {*}
     * @memberof Build
     */
    filepath?: any;

    /**
     * 完成的需求
     *
     * @returns {*}
     * @memberof Build
     */
    stories?: any;

    /**
     * 解决的Bug
     *
     * @returns {*}
     * @memberof Build
     */
    bugs?: any;

    /**
     * 打包日期
     *
     * @returns {*}
     * @memberof Build
     */
    date?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Build
     */
    product?: any;

    /**
     * 平台/分支
     *
     * @returns {*}
     * @memberof Build
     */
    branch?: any;

    /**
     * 所属项目
     *
     * @returns {*}
     * @memberof Build
     */
    project?: any;

    /**
     * 产品名称
     *
     * @returns {*}
     * @memberof Build
     */
    productname?: any;
}