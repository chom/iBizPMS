/**
 * 产品计划
 *
 * @export
 * @interface ProductPlan
 */
export interface ProductPlan {

    /**
     * 名称
     *
     * @returns {*}
     * @memberof ProductPlan
     */
    title?: any;

    /**
     * 编号
     *
     * @returns {*}
     * @memberof ProductPlan
     */
    id?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof ProductPlan
     */
    begin?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof ProductPlan
     */
    desc?: any;

    /**
     * 结束日期
     *
     * @returns {*}
     * @memberof ProductPlan
     */
    end?: any;

    /**
     * 已删除
     *
     * @returns {*}
     * @memberof ProductPlan
     */
    deleted?: any;

    /**
     * 排序
     *
     * @returns {*}
     * @memberof ProductPlan
     */
    order?: any;

    /**
     * 父计划名称
     *
     * @returns {*}
     * @memberof ProductPlan
     */
    parentname?: any;

    /**
     * 平台/分支
     *
     * @returns {*}
     * @memberof ProductPlan
     */
    branch?: any;

    /**
     * 父计划
     *
     * @returns {*}
     * @memberof ProductPlan
     */
    parent?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof ProductPlan
     */
    product?: any;
}