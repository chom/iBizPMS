import { ControlBase } from './ControlBase';

/**
 * 部件基础公共基类
 *
 * @export
 * @class MainControlBase
 * @extends {ControlBase}
 */
export class MainControlBase extends ControlBase {

}