export default {
  fields: {
    diff: "不同",
    field: "字段",
    ibiznew: "新值",
    old: "旧值",
    id: "id",
    action: "关联日志",
  },
};