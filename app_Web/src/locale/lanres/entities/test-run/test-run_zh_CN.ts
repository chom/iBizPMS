export default {
  fields: {
    lastrunresult: "结果",
    lastrundate: "最后执行时间",
    assignedto: "指派给",
    lastrunner: "最后执行人",
    status: "当前状态",
    id: "编号",
    version: "用例版本",
    ibizcase: "测试用例",
    task: "测试单",
  },
};