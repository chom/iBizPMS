
export default {
  fields: {
    extra: "附加值",
    objecttype: "对象类型",
    id: "id",
    comment: "备注",
    read: "已读",
    action: "动作",
    date: "日期",
    product: "产品",
    objectid: "对象ID",
    actor: "操作者",
    project: "项目",
  },
	views: {
		historylistview: {
			caption: "系统日志",
      		title: "历史记录",
		},
		projecttrendslistview: {
			caption: "系统日志",
      		title: "系统日志列表视图",
		},
		projecttrendslistview9: {
			caption: "系统日志",
      		title: "产品动态",
		},
		producttrendslistview9: {
			caption: "系统日志",
      		title: "产品动态",
		},
		alltrendslistview: {
			caption: "系统日志",
      		title: "系统日志列表视图",
		},
		producttrendslistview: {
			caption: "系统日志",
      		title: "系统日志列表视图",
		},
		editview: {
			caption: "系统日志",
      		title: "action编辑视图",
		},
	},
	main_form: {
		details: {
			group1: "action基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srforikey: "", 
			srfkey: "id", 
			srfmajortext: "备注", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			id: "id", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
	},
};